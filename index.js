var app = require('express')();
var server = require('http').Server(app);

var io = require('socket.io')(server); // socket itself

var Redis = require('ioredis');
var redis = new Redis();

redis.subscribe('test-channel');

redis.on('message',function(channel,message) {
    message = JSON.parse(message);
    console.log(channel+':'+message.trigger);
    io.emit(channel+':'+message.trigger,message.message);
});

server.listen(8000,function(){ // node server ready
    console.log('ready');
});

app.get('/', function (request,response) {
    response.sendFile(__dirname + '/public/test.html')
});

io.on('connection',function (socket) { // succesfull connection from a client

    socket.on('test-channel:chat',function(message){ // listner from clients RECEIVER
        io.emit('test-channel:chat',message); // broadcastV SENDER
    });

    // PFR
    socket.on('pfr-channel:track',function(message){ // listner from clients RECEIVER
        io.emit('pfr-channel:record',message); // broadcastV SENDER
    });

    socket.on('disconnect',function(){ // listner from clients when disconnects  // DEFAULT
        io.emit('test-channel:chat','User has been disconnected'); // broadcast
    });
});
