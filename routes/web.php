<?php

use Illuminate\Support\Facades\Redis;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {


    $data = [
        'trigger'=>'chat',
        'message'=> 'ADMIN MESSAGE'
    ];

    Redis::publish('test-channel',json_encode($data));

});


Route::get('/test','NotifyController@index');


Route::get('/lols',function(){
    return view('test.html');
});

Route::get('/socket-enable', function(){
    shell_exec('node ' . __DIR__ . '/../index.js');
});

Route::get('/socket-disable', function(){
    shell_exec('pkill -f "node ' . __DIR__ . '/../index.js"');
});
